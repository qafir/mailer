#!/usr/bin/env python3

import os
import sys
import unittest

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

import mailer


class AddressTestCase(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    cls.name = 'Test Account'
    cls.mail = 'test@example.com'

  def test_str(self):
    name = self.__class__.name
    mail = self.__class__.mail

    addr1 = mailer.Address(mail)
    addr2 = mailer.Address(mail, name)

    self.assertEqual(str(addr1), '<%s>' % mail)
    self.assertEqual(str(addr2), '%s <%s>' % (name, mail))

  def test_eq(self):
    name = self.__class__.name
    mail = self.__class__.mail

    addr1 = mailer.Address(mail, name)
    addr2 = mailer.Address('', name)
    addr3 = mailer.Address(name, mail)
    addr4 = mailer.Address(mail, name)

    self.assertTrue(addr1 == addr4)
    self.assertFalse(addr1 == addr2)
    self.assertFalse(addr1 == addr3)
    self.assertFalse(addr1 == name)
    self.assertTrue(addr1 == mail)
    self.assertTrue(addr1 == '<%s>' % mail)
    self.assertTrue(addr1 == '%s <%s>' % (name, mail))


if __name__ == '__main__':
  unittest.main()
