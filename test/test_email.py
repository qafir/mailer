#!/usr/bin/env python3

import os
import sys
import io
import unittest

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

import mailer


class EmailTestCase(unittest.TestCase):
  @staticmethod
  def build_email(attachments=None):
    sender = mailer.Address('test@example.com')
    recipients = (
      mailer.Address('testr1@example.com'),
      mailer.Address('testr2@example.com', 'Recipient 2'),
    )
    content = 'Some random text'
    return mailer.Email(sender, recipients, content, attachments)

  def test_to_mime(self):
    attachments = (
      mailer.Attachment('attachment1', io.StringIO('Some text')),
      mailer.Attachment('attachment2', io.StringIO('Some text 2')),
      mailer.Attachment('attachment3', io.StringIO('Some text 3')),
    )
    email = self.__class__.build_email()
    email_attachments = self.__class__.build_email(attachments)

if __name__ == '__main__':
  unittest.main()
